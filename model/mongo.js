class Mongo {}

var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://52.78.234.20:27017/sadajo';

Mongo.createRoom = (room, callback) => {console.log('>>> [Mongo] createRoom in');
	
	console.log('>>> [Mongo] room : ', room );
	
	//연결
	MongoClient.connect(url, function(err, db) {
		
		if (err) {
	      console.error('>>> [Mongo] MongoDB 연결 실패', err);
	      return callback(err);
	    }

		console.log(">>> [Mongo] MongoDB 연결 성공");
		
		//var chat = db.collection('chat');
		var chat = db.collection('chat');
		chat.insert({room: room, message:[]},
			function(err,result) {
				if (err) {
		            console.error('>>> [Mongo] Insert Error', err);
		            return callback(err);

        		}
        		console.log('>>> [Mongo] Insert success');
        		db.close();
        		callback(null, result);

			}
		);

	});

};

Mongo.saveMsg = (room, sender, msg, callback) => {console.log('>>> [Mongo] saveMsg in');
	
	console.error('>>> [Mongo] room : ', room, ' sender : ', sender, ' msg : ', msg);
	
	//연결
	MongoClient.connect(url, function(err, db) {
		
		if (err) {
	      console.error('>>> [Mongo] MongoDB 연결 실패', err);
	      return;
	    }

		console.log(">>> [Mongo] MongoDB 연결 성공");
		
		var today = new Date();
		var h = today.getHours()+9;console.log('>>> [Mongo] h : ',h);
		var m = today.getMinutes();
		var s = today.getSeconds();
		var y = today.getFullYear();
		var month = today.getMonth()+1;
		var day = today.getDate();
		if(m<10){
			m='0'+m;
		}
		if(h>24){
				h=h-24;
		}
		var d = y+'-'+ month+'-'+day;
		var time = h+':'+m;console.log('>>> [Mongo] time : ',time);

		var chat = db.collection('chat');
		chat.update({room: room}, {$push :{message:{sender:sender, msg:msg, date:d, time:time}}}, 

			function(err, result) {
				if (err) {
		            console.error('>>> [Mongo] Update Error', err);
		            return;
        		}
        		console.log('>>> [Mongo] Update success');
        		db.close();
        		callback(null, {date:d, time:time});
			}
		);

	});

};


Mongo.getMsg = (room, callback) => {console.log('>>> [Mongo] getMsg in! room : ', room);
	
	var none = [];
	if(!room) { callback(1, null); }
	
	//연결
	MongoClient.connect(url, function(err, db) {
		
		if (err) {
	      console.error('>>> [Mongo] MongoDB 연결 실패', err);
	      return;
	    }

		console.log(">>> [Mongo] MongoDB 연결 성공");
		
		var room1= parseInt(room);
		var chat = db.collection('chat');
		chat.findOne({room:room1},{_id:0, room:0},
			function (err, docs) {

				if (err) {
       				console.error('>>> [Mongo] Find Error : ', err);
         			return;
      			}
      			console.log('>>> [Mongo] Find message : ', docs);
      			db.close();
      			callback(null, docs);
   			}); 

	});

};


module.exports = Mongo;