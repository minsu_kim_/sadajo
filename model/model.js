var mysql = require('mysql');

class Model {}

var pool1 = mysql.createPool({
  host     : 'sadajodb.cxik0qkaghgk.ap-northeast-2.rds.amazonaws.com',
  port     : '3306',
  user     : 'sadajo',
  password : 'sadajo123',
  database : 'sadajo'

});

/*var pool1 = mysql.createPool({
	host    : 'dbinstance.c9umtysgztsq.ap-northeast-2.rds.amazonaws.com',
    port : 3306,
	user : 'admin',
	password : 'adminpw1',
	database : 'sadajoDB'
});*/

Model.getToken = (user, callback) => {console.log('>>> [Sql] getToken in! user : ', user);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select count(*), fcm_token token from user where user_code = ?';
		conn.query(sql, [user], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getToken error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getToken success! room : ',result[0]);
			conn.release();            
            callback(null, result[0]);

		});
	});

};


Model.getUserpage = (id, callback) => {console.log('>>> [Sql] getUserpage in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select (select count(*) from request where req_user = ?) req_num, (select count(*) from request where carr_user = ?) carr_num, user_code code, user_nick nick, user_img img, ifnull(location, \'한국\') location from user where user_code = ?';
		conn.query(sql, [id, id, id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getRoom error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getRoom success! room : ',result[0]);
			conn.release();            
            callback(null, result[0]);

		});
	});
};


Model.getRoom = (callback) => {console.log('>>> [Sql] getRoom in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select max(room_num) room from chatroom';
		conn.query(sql, (err, result) => {

			if(err) {
				console.log('>>> [Sql] getRoom error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getRoom success! room : ',result[0]);
			conn.release();            
            callback(null, result[0]);

		});
	});

};


Model.insertRoom = (data, callback) => {console.log('>>> [Sql] insertRoom in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'insert into chatroom (room_num, req_user, carr_user) values (?,?,?)';
		conn.query(sql, [data.room, data.req, data.carr], (err, result) => {

			if(err) {
				console.log('>>> [Sql] insertRoom error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] insertRoom success! insertID : ',result.insertId);
            var returnData = {
                insertId : result.insertId
            }

            conn.release();
            callback(null, returnData);

		});
	});
};


Model.insertRequest = (data, callback) => {console.log('>>> [Sql] insertRequest in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'insert into request (req_user, carr_user, goods_name, price, detail, country_code) values (?,?,?,?,?,?)';
		conn.query(sql, [data.req, data.carr, data.goods, data.price, data.detail, data.country], (err, result) => {

			if(err) {
				console.log('>>> [Sql] insertRequest error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] insertRequest success! insertID : ',result.insertId);
            var returnData = {
                insertId : result.insertId
            }

            conn.release();
            callback(null, returnData);

		});
	});
};


Model.insertSchedule = (data, callback) => {console.log('>>> [Sql] insertSchedule in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'insert into shoplist (user_code, country_code, city_code, start_date, end_date) values (?,?,?,?,?)';
		conn.query(sql, [data.user, data.country, data.city, data.start, data.end], (err, result) => {

			if(err) {
				console.log('>>> [Sql] insertSchedule error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] insertSchedule success! insertID : ',result.insertId);
            var returnData = {
                insertId : result.insertId
            }

            conn.release();
            callback(null, returnData);

		});
	});
};


Model.getHashtag = (code, callback) => {console.log('>>> [Sql] getHashtag in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select tag, goods_code from recommend where country_code = ?';

		conn.query(sql, [code], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getHashtag error : ', err);
				conn.release();
				return callback(err);
			}
			console.log('>>> [Sql] getHashtag success');
			conn.release();
			callback(null, result);

		});
	});
};


Model.getShoplistCountry = (code, callback) => {console.log('>>> [Sql] getShoplistCountry in');

	pool1.getConnection( (err, conn) => {

		var sql = 'select distinct user_nick as nickname, shoplist.user_code, user_img from shoplist join user on shoplist.user_code = user.user_code where country_code =? order by goods_num desc limit 8'
		conn.query(sql, [code], (err, result) => {

			if(err){
				console.log('>>> [Sql] getShoplistCountry error : ', err);
				conn.release();
				return callback(err);
			}

			conn.release();
			callback(null, result);

		});
	});
};


Model.getScheduleCount = (id, callback) => {console.log('>>> [Sql] model.getScheduleCount in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select count(*) as count from shoplist where user_code=?';
		var count = 0;
		
		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getScheduleCount error : ', err);
				conn.release();
				return callback(err);
			}

			count = result[0].count;			
			console.log('>>> [sql] getScheduleCount success! count : ', count);
			conn.release();
			callback(null, count);

		});
	});
};


Model.getLastSchedule = (id, callback) => {console.log('>>> [Sql] model.getLastSchedule in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select shoplist.country_code, country_name_kor as country_name, country_name_eng, country.img as country_img, end_date, start_date from shoplist join country on shoplist.country_code = country.country_code where user_code=? order by end_date desc limit 1';
		var end_date = '';

		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getLastSchedule error : ', err);
				conn.release();
				return callback(err);
			}
			
			console.log('>>> [sql] getLastSchedule success! end_date : ', end_date);
			end_date = result[0].end_date;
			conn.release();
			callback(null, result);

		});
	});
};



Model.getSchedule = (id, callback) => {console.log('>>> [Sql] getSchedule in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select country_code from shoplist where user_code=? order by end_date';
		var title_country = '';

		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getSchedule error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getSchedule success! data : ', result);
			conn.release();
			callback(null, result);

		});
	});
};


Model.getShoplist = (id, callback) => {console.log('>>> [Sql] getShoplist in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select a.country_name_kor, a.country_name_eng, b.city_name_kor, start_date, end_date, goods_num, s.img, list_code from shoplist s left join country a on s.country_code = a.country_code left join city b on s.city_code = b.city_code where user_code = ? order by end_date desc';

		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getShoplist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getShoplist success! data : ', result);
			conn.release();

			//날짜 형식을 yyyy-mm-dd 로 바꿈
			for (var i=0; i<result.length; i++) {
				var start = result[i].start_date;
				var end = result[i].end_date;
				start = start.toISOString().substring(0, 10);
				end = end.toISOString().substring(0, 10);
				result[i].start_date = start;
				result[i].end_date = end;
			}
			console.log('>>> [Sql] getShoplist success! data after : ', result);
			callback(null, result);

		});
	});
};


Model.getLikelist = (id, callback) => {console.log('>>> [Sql] getLikelist in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select a.country_name_kor, a.country_name_eng, goods_num, l.img, list_code from likelist l left join country a on l.country_code = a.country_code where user_code = ?';

		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getLikelist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getLikelist success! data : ', result);
			conn.release();			
			callback(null, result);

		});
	});
};


Model.getChatlist = (id, callback) => {console.log('>>> [Sql] getChatlist in!'); console.log('>>> [Sql] getChatlist id : ', id);

	pool1.getConnection( (err, conn) => {

		var sql = 'SELECT room_num room, req_user req, carr_user carr, DATE_FORMAT(regdate, \'%Y-%m-%d\') lastdate, (select user_nick from user where user_code = req_user) req_nick, (select user_nick from user where user_code = carr_user) carr_nick, (select user_img from user where user_code = req_user) req_img, (select user_img from user where user_code = carr_user) carr_img from chatroom where req_user = ? or carr_user = ? order by regdate desc';

		conn.query(sql, [id, id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getShoplist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getShoplist success! data : ', result);
			conn.release();
			callback(null, result);

		});
	});
};


Model.getlikedetail = (code, callback) => {console.log('>>> [Sql] getlikedetail in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select distinct l.goods_code, g.goods_name, g.img, c.country_name_kor country, c.img country_img from likegoods l join goods g on l.goods_code = g.goods_code join country c on MID(l.goods_code,1,3) = c.country_code where list_code = ? order by l.regdate desc';

		conn.query(sql, [code], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getlikedetail error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getlikedetail success! data : ', result);
			conn.release();
			callback(null, result);

		});
	});

};


Model.getshopdetail = (code, callback) => {console.log('>>> [Sql] getshopdetail in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select distinct s.goods_code, g.goods_name, g.img, c.country_name_kor country, c.img country_img from shopgoods s join goods g on s.goods_code = g.goods_code join country c on MID(s.goods_code,1,3) = c.country_code where list_code = ? order by s.regdate desc';

		conn.query(sql, [code], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getshopdetail error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getshopdetail success! data : ', result);
			conn.release();
			callback(null, result);

		});
	});

};


Model.checkLikelist = (data, callback) => {console.log('>>> [Sql] checkLikelist in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select count(list_code) count, list_code  from likelist where user_code = ? and country_code = ?'

		conn.query(sql, [data.user, data.country], (err, result) => {

			if(err) {
				console.log('>>> [Sql] checkLikelist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] checkLikelist success! data : ', result);
			conn.release();
			callback(null, result);

		});
	});
}


Model.checkShoplist = (data, callback) => {console.log('>>> [Sql] checkShoplist in!');

	pool1.getConnection( (err, conn) => {

		var sql = 'select count(list_code) count, list_code from shoplist where user_code = ? and country_code = ?'

		conn.query(sql, [data.user, data.country], (err, result) => {

			if(err) {
				console.log('>>> [Sql] checkShoplist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] checkShoplist success! data : ', result);
			conn.release();
			callback(null, result);

		});
	});
}


Model.insertLikelist = (data, callback) => {console.log('>>> [Sql] insertLikelist in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'insert into likelist (user_code, country_code) values (?,?)';
		conn.query(sql, [data.user, data.country], (err, result) => {

			if(err) {
				console.log('>>> [Sql] insertLikelist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] insertLikelist success! insertID : ',result.insertId);
            var returnData = {
                insertId : result.insertId
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.insertShopGoods = (data, callback) => {console.log('>>> [Sql] insertShopGoods in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'insert into shopgoods (goods_code, list_code) values (?,?)';
		conn.query(sql, [data.goods, data.list], (err, result) => {

			if(err) {
				console.log('>>> [Sql] insertShopGoods error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] insertShopGoods success! insertID : ',result.insertId);
            var returnData = {
                insertId : result.insertId
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.insertGoods = (data, callback) => {console.log('>>> [Sql] insertGoods in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'insert into likegoods (goods_code, list_code) values (?,?)';
		conn.query(sql, [data.goods, data.list], (err, result) => {

			if(err) {
				console.log('>>> [Sql] insertGoods error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] insertGoods success! insertID : ',result.insertId);
            var returnData = {
                insertId : result.insertId
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.updateShoplistImg = (data, callback) => {console.log('>>> [Sql] updateShoplistImg in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update shoplist set img = (select img from goods where goods_code = ?) where list_code = ?';
		conn.query(sql, [data.goods, data.list], (err, result) => {

			if(err) {
				console.log('>>> [Sql] updateShoplistImg error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] updateShoplistImg success! insertID : ',result);
            var returnData = {
                insertId : result
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.updateLikelistImg = (data, callback) => {console.log('>>> [Sql] updateLikelistImg in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update likelist set img = (select img from goods where goods_code = ?) where list_code = ?';
		conn.query(sql, [data.goods, data.list], (err, result) => {

			if(err) {
				console.log('>>> [Sql] updateLikelistImg error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] updateLikelistImg success! insertID : ',result);
            var returnData = {
                insertId : result
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.updateShopGoodsNum = (data, callback) => {console.log('>>> [Sql] updateShopGoodsNum in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update shoplist set goods_num = (select count(*) from shopgoods where list_code = ? ) where list_code = ? ';
		conn.query(sql, [data.list, data.list], (err, result) => {

			if(err) {
				console.log('>>> [Sql] updateShopGoodsNum error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] updateShopGoodsNum success! update msg : ',result.message);
            var returnData = {
                msg : result.message
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.updateLikeGoodsNum = (data, callback) => {console.log('>>> [Sql] updateLikeGoodsNum in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update likelist set goods_num = (select count(*) from likegoods where list_code = ? ) where list_code = ? ';
		conn.query(sql, [data.list, data.list], (err, result) => {

			if(err) {
				console.log('>>> [Sql] updateLikeGoodsNum error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] updateLikeGoodsNum success! update msg : ',result.message);
            var returnData = {
                msg : result.message
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.updateShopNum = (data, callback) => {console.log('>>> [Sql] updateShopNum in! data : ', data);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update goods set shop_num = shop_num + 1 where goods_code = ? ';
		conn.query(sql, [data, data], (err, result) => {

			if(err) {
				console.log('>>> [Sql] updateShopNum error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] updateShopNum success! update msg : ',result.message);
            var returnData = {
                msg : result.message
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.updateLikeNum = (data, callback) => {console.log('>>> [Sql] updateLikeGoodsNum in! data : ', data);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update goods set like_num = like_num + 1 where goods_code = ? ';
		conn.query(sql, [data, data], (err, result) => {

			if(err) {
				console.log('>>> [Sql] updateLikeGoodsNum error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] updateLikeGoodsNum success! update msg : ',result.message);
            var returnData = {
                msg : result.message
            };

            conn.release();
            callback(null, returnData);

		});
	});
}


Model.getUserInfo = (user, callback) => {console.log('>>> [Sql] getUserInfo in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select count(*) count, user_nick nick, user_img img from user where user_code = ?';
		conn.query(sql, [user], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getUserInfo error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getUserInfo success! result : ',result);
            conn.release();
            callback(null, result);

		});
	});
}


Model.getPartner = (room, callback) => {console.log('>>> [Sql] getPartner in!');

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select count(*) count, req_user req, carr_user carr from chatroom where room_num = ?';
		conn.query(sql, [room], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getPartner error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getPartner success! result : ', result);
            conn.release();
            callback(null, result);

		});
	});
}


Model.getReqlist = (id, callback) => {console.log('>>> [Sql] getReqlist in! owner : ', id);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select c.country_name_eng country, c.img2 img, u.user_img carr_img, u.user_nick nick, DATE_FORMAT(req_date, \'%Y-%m-%d\') thedate, goods_name, req_code, status from request r left join country c on c.country_code = r.country_code left join user u on u.user_code = r.carr_user where req_user = ? order by req_date desc';
		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getReqlist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getReqlist success! room : ',result);
            conn.release();
            callback(null, result);

		});
	});
};


Model.getCarrlist = (id, callback) => {console.log('>>> [Sql] getCarrlist in! owner : ', id);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select c.country_name_eng country, c.img2 img, u.user_img req_img, u.user_nick nick, DATE_FORMAT(req_date, \'%Y-%m-%d\') thedate, goods_name, req_code, status from request r left join country c on c.country_code = r.country_code left join user u on u.user_code = r.req_user where carr_user = ? order by req_date desc';
		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getCarrlist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getCarrlist success! room : ',result);
            conn.release();
            callback(null, result);

		});
	});
};


Model.getLikeCount = (data, callback) => {console.log('>>> [Sql] getLikeCount in! data : ', data);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select like_num from goods where goods_code = ?';
		conn.query(sql, [data.goods], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getLikeCount error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getLikeCount success! room : ',result);
            conn.release();
            callback(null, result);

		});
	});

};


Model.getShopCount = (data, callback) => {console.log('>>> [Sql] getShopCount in! data : ', data);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select shop_num from goods where goods_code = ?';
		conn.query(sql, [data.goods], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getShopCount error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getShopCount success! room : ',result);
            conn.release();
            callback(null, result);

		});
	});

};


Model.getReview = (id, callback) => {console.log('>>> [Sql] getReview in! owner : ', id);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select r.content, (select u.user_img from user u where user_code = ?) img, (select u.user_nick from user u where user_code = ?) nick, req.country_code country, req.goods_name, DATE_FORMAT(r.regdate, \'%Y-%m-%d\') regdate, r.req_code from review r left join request req on r.req_code = req.req_code where r.writer = ?';
		conn.query(sql, [id, id, id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getReqlist error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getReqlist success! room : ',result);
            conn.release();
            callback(null, result);

		});
	});
};


Model.getMytip = (id, callback) => {console.log('>>> [Sql] getMytip in! owner : ', id);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select r.content, u.user_img img, DATE_FORMAT(r.regdate, \'%Y-%m-%d\') regdate, r.goods_code from tips r left join user u on r.user_code = u.user_code where r.user_code = ?';
		conn.query(sql, [id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] getMytip error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] getMytip success! room : ',result);
            conn.release();
            callback(null, result);

		});
	});
};


Model.checkGoods = (list, goods, callback) => {console.log('>>> [Sql] checkGoods in! goods : ', goods, ' list : ', list);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select count(*) count from likegoods where list_code = ? and goods_code = ?';
		conn.query(sql, [list, goods], (err, result) => {

			if(err) {
				console.log('>>> [Sql] checkGoods error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] checkGoods success! result : ', result[0]);
			conn.release();            
            callback(null, result[0]);

		});
	});

};


Model.checkGoods2 = (list, goods, callback) => {console.log('>>> [Sql] checkGoods2 in! goods : ', goods, ' list : ', list);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'select count(*) count from shopgoods where list_code = ? and goods_code = ?';
		conn.query(sql, [list, goods], (err, result) => {

			if(err) {
				console.log('>>> [Sql] checkGoods2 error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] checkGoods2 success! result : ', result[0]);
			conn.release();            
            callback(null, result[0]);

		});
	});

};


Model.saveToken = (id, token, callback) => {console.log('>>> [Sql] saveToken in! id : ', id, ' token : ', token);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update user set fcm_token = ? where user_code = ? ';
		conn.query(sql, [token, id], (err, result) => {

			if(err) {
				console.log('>>> [Sql] saveToken error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] saveToken success! result : ', result);
			conn.release();            
            callback(null, result);

		});
	});

};



Model.sayYes = (code, callback) => {console.log('>>> [Sql] sayYes in! code : ', code);

	pool1.getConnection( (err, conn) => {

		if ( err ){
            console.log('Connection Error : ', err);
            return callback(err);
        }

        var sql = 'update request set status = 1 where req_code = ? ';
		conn.query(sql, [code], (err, result) => {

			if(err) {
				console.log('>>> [Sql] sayYes error : ', err);
				conn.release();
				return callback(err);
			}

			console.log('>>> [Sql] sayYes success! result : ', result);
			conn.release();            
            callback(null, result);

		});
	});

};


module.exports = Model;