var pool = require('./dbConnect');
var express = require('express');
var router = express.Router();




//검색 초기 화면 
//쿼리스트링으로 상품코드 입력하면 
//해당 상품코드 정보가 나옴
//goods?goodscode=usa
//goods?hashtag = ""
//goods?country = ""
//goods?name = ""
router.route('/goods')
        .get(showGoodslist);



//id 로 검색 하면 세부 데이터가 나옴
//goods/:상품코드
//해쉬태그, 가격, 판매처, 팁
router.route('/goods/:goods_code')
        .get(showGoodsDetail);



//전체 팁 검색
router.route('/tips')
        .get(showTiplist);
        
router.route('/tips/:goods_code')
        .get(showTipDetail)

//팁 추가 
        .post(addTip);




//전체 나라 코드 검색
router.route('/country')
        .get(showCountrylist);

/************************************************************* */

// function editTip(req,res,next){
//     var goods_code = req.params.goods_code;
//     var user_code = req.params.user_code;
//     var content = req.params.content;
// }







//content
function addTip(req, res, next){
    var goods_code = req.params.goods_code;
    var user_code = req.body.user_code;
     var content = req.body.content;

    pool.getConnection(function(err, conn){
                 if(err){
                     err.code = 500;
                     return next(err);
                    }

                    var sql = 'insert into tips set ?';
                   
                    var info = {
                        goods_code : goods_code,
                        user_code : user_code,
                        content: content
                    };

            conn.query(sql, info, function(err, results){
                       
                    if(err){
                     err.code = 500;
                     return next(err);
                    }


                    var sql2 = 'select tips.user_code, tips.writer, tips.content from tips where tips.goods_code = ?';
                    conn.query(sql2, [goods_code], function(err, results){
                              if(err){
                            err.code = 500;
                            return next(err);
                            }

                            var list = {
                                count : results.length,
                                data: results
                            }
                             res.send(list);
                             conn.release();
                    });
                    
            });
            console.log(addTip);
    });
}





function showGoodslist(req, res, next){
    http://localhost:3000/goods&hashtag="화장품"
    var goodscode = req.query.goodscode;
    var country = req.query.country;
    var hashtag = req.query.hashtag;
    var name = req.query.name;
    var date = req.query.date;
    var click = req.query.click;

    console.log('검색 초기 화면 ', goodscode);
    console.log('검색 초기 화면 ', country);
    console.log('검색 초기 화면 ', hashtag);

    pool.getConnection(function(err, conn){
               
                if(err){
                err.code = 500;
                return next(err);
                }
        
        var where = 'where ';
        var order = '';

        //최신순
        if(date){
            order += ' order by regdate desc';
        }
        //조회순
        else if(click){
            order += ' order by click desc';
        }
        /*********************************************************** */
        
        //상품코드
        if(goodscode){
            where += ' goods.goods_code like "%' + goodscode + '%" ';
       

        }

        
        //나라별
        
        if(country){
               
            where += ' goods.country like "%' + country + '%" ';
        }else{
            where += '1=1 ';
        }


        if(hashtag){
            where += 'and (goods.hashtag like "%' + hashtag + '%" or goods.goods_name like "%' + name + '%" )';
        }
        
        console.log(where);
        //     //해쉬태그 
        //     if ( where.length > 0 ){
        //          where += 'and';
        //          where += '(';
                
        //           if(hashtag){
        //             if ( where.length > 0 ){
        //                     where += 'or';
        //                 }else{
        //                     where +='';
        //                 }
        //             where += ' goods.hashtag like "%' + hashtag + '%" ';

        //         }
        //         //상품이름 
        //         if(name){
        //              if ( where.length > 0 ){
        //                     where += 'or';
        //                 }else{
        //                     where +='';
        //                 }
        //             where += ' goods.goods_name like "%' + name + '%" ';
                   
        //             where += ')';
            

        //     }
        // }
        
        
     
       
        var sql = 'select goods.id, goods.img goods_img, goods.goods_code, goods.goods_name, goods.hashtag ,goods.country, country.img country_img, country.img2 country_img2, goods.click, DATE_FORMAT(goods.regdate, \'%Y-%m-%d\') regdate from goods left outer join country on goods.country_code = country.country_code '+ where + order;

        console.log('>>> sql : ', sql);
       
        conn.query(sql, function(err, results){
                    
                    if(err){
                     err.code = 500;
                     return next(err);
                    }


                    if(err){
                        return res.status(500).send({MSG: err.message});
                    }                    
                    var list = {
                        count : results.length,
                        data  : results
                       
                    };

                   res.send(list);
                  conn.release();
                
                
        });
         
        
    });
    console.log('success');

};

/************************************************************* */


function showGoodsDetail(req, res, next){
  var goods_code = req.params.goods_code;
  
  console.log(goods_code);
  var goodsInfo = {};

  pool.getConnection(function(err, conn){
    var sql = 'select id, unit ,goods_code, goods_name, country, click, DATE_FORMAT(regdate, \'%Y-%m-%d\') regdate, content, like_num, shop_num from goods where goods.goods_code = ?';
    conn.query(sql, [goods_code], function(err, results){

        if(err){
        err.code = 500;
        return next(err);
      }
      
      console.log('results.length', results.length, results);
      console.log('good',goodsInfo.length);
    //   if(goodsInfo.length > 0){
    //         goodsInfo = results[0];
    //     }else{
    //         goodsInfo={};
    //     }
        if(results.length !=0 ){
                goodsInfo = results[0];
        }

       
      //해시태그 배열 
      var sql2 = 'select goods.goods_name, hashtag.hashtag from goods inner join hashtag on goods.goods_code = hashtag.goods_code where goods.goods_code = ?';
      conn.query(sql2, [goods_code], function(err, results){
                        if(err){
                        err.code = 500;
                        return next(err);
                    }

                    var hashtag =[];
                        for(var i=0; i< results.length; i++){
                            hashtag.push(results[i].hashtag);
                        }

                    goodsInfo.hashtag = hashtag;

         //파는 장소 배열
       var sql3 = 'select goods.goods_name, sellplace.sell_place from goods inner join sellplace on goods.goods_code = sellplace.goods_code where goods.goods_code =? ';
      conn.query(sql3, [goods_code], function(err, results){
        if(err){
        err.code = 500;
        return next(err);
        }
                        var sell_place =[];
                        for(var i=0; i< results.length; i++){
                            sell_place.push(results[i].sell_place);
                        }
                    goodsInfo.sell_place = sell_place;
                    
                
            //price 배열
            var sql4 = 'select goods.goods_name, price.tag_price, price.price from goods inner join price on goods.goods_code = price.goods_code where goods.goods_code =? ';
      conn.query(sql4, [goods_code], function(err, results){
        if(err){
        err.code = 500;
        return next(err);
        }
                    var tag_price=[];
                        for(var i=0; i< results.length; i++){
                            tag_price.push(results[i].tag_price);
                        }
                    var price =[];
                        for(var i=0; i< results.length; i++){
                            price.push(results[i].price);
                        }
                    goodsInfo.tag_price = tag_price;               
                    goodsInfo.price = price;
                
       
         //tips 배열 DATE_FORMAT(tips.regdate, \'%Y-%m-%d-%T\')
              var sql5 = 'select user.user_img, user.user_nick user_name, tips.content, DATE_FORMAT(tips.regdate,  \'%Y-%m-%d\') regdate from goods inner join tips on goods.goods_code = tips.goods_code inner join user on user.user_code = tips.user_code where goods.goods_code =? ';
               conn.query(sql5, [goods_code], function(err, results){
                    if(err){
                    err.code = 500;
                    return next(err);
                    }
                      
                         goodsInfo.tips= results;
                       
                         //이미지
             var sql6 = 'select goodsImage.goods_img from goods join goodsImage on goods.goods_code = goodsImage.goods_code where goods.goods_code = ?';
            conn.query(sql6, [goods_code], function(err, results){
             if(err){
                 err.code = 500;
                      return next(err);
                 }
                      
                      var goods_img=[];
                        for(var i=0; i< results.length; i++){
                            goods_img.push(results[i].goods_img);
                        }       
         
                        goodsInfo.goods_img = goods_img;
                    
                 
                  


                              //쇼퍼맨
                     var sql7 = 'select user.user_img, user.user_code, user.user_id, user.user_nick user_name, user.location from user join goods on user.location = goods.country_code where goods.goods_code = ?';
                    conn.query(sql7, [goods_code], function(err, results){
                            if(err){
                            err.code = 500;
                            return next(err);
                            }
                            console.log('>>> [goods] result : ',results);            
                            goodsInfo.shoperman = results;
                            res.send(goodsInfo);
                            conn.release();

                  

      
                                  });

      
                             });







                     });
        
    
            
        
            });
      
      });

     

 });

    

     

    

    });
  });
console.log(showGoodsDetail);
};

/************************************************************* */

// 전체 팁 검색
function showTiplist(req, res, next){
  pool.getConnection(function(err, conn){

    if(err){
      err.code = 500;//서버 오류
      return next(err);
    }

    var sql = 'select tips.tip_code, tips.goods_code, user.user_nick user_name, user.user_img, tips.content, tips.regdate  from tips join user on tips.user_code = user.user_code';
    conn.query(sql, function(err, results){
      if(err){
        err.code = 500;
        return next(err);
      }
  var tipList = {
        count: results.length,
        data: results
      };

      conn.release();
      res.send(tipList);

    });
  });
  console.log(showTiplist);
};



/*************************************************************/
//팁 상세정보 
function showTipDetail(req, res, next){
  var goods_code = req.params.goods_code;
 
  pool.getConnection(function(err, conn){

    if(err){
      err.code = 500;//서버 오류
      return next(err);
    }

    var sql = 'select tip_code, tips.goods_code, user.user_code, user.user_nick writer, user.user_img, tips.content, DATE_FORMAT(regdate, \'%Y-%m-%d\') regdate from tips join user on user.user_code = tips.user_code where goods_code = ?';
    // var sql = 'select * from tips where goods_code = ?';


    conn.query(sql, [goods_code], function(err, results){
        if(err){
        err.code = 500;
        return next(err);
      }
  
    var tipList = {
        count : results.length,
        data : results
    };
        conn.release();
        res.send(tipList);



    });
      console.log(showTipDetail);

});

}
/************************************************************* */


/************************************************************* */
//전체 나라 검색
function showCountrylist(req, res, next){
  pool.getConnection(function(err, conn){
    if(err){
      err.code = 500;

      return next(err);
    }

  var sql = 'select  * from country ';
    conn.query(sql, function(err, results){
      if(err){
        err.code = 500;
        return next(err);
      }

      var countryList = {
        count: results.length,
        data: results
      };

      conn.release();
      res.send(countryList);

    });











  });
  console.log(showCountrylist);
};
/************************************************************* */

module.exports = router;


