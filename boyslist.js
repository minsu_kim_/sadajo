var pool = require('./dbConnect');
var express = require('express');
var router = express.Router();

router.route('/boys')
        .get(showboyslist);

router.route('/boys')
        .post(addboys);



function showboyslist(req,res,next){
    
    pool.getConnection(function(err,conn){

     if(err){
        err.code = 500;//서버 오류
        return next(err);
         }
         
        var sql = 'select * from boys'; 
         conn.query(sql, function(err, results){
              var boysList = {
                    count: results.length,
                    data: results
                 };

                 conn.release();
                 res.send(boysList);

         });    


        });
       
    }
/************************************************************** */
//content
function addboys(req, res, next){
    var id = req.params.id;
    var name = req.body.name;
     var age = req.body.age;

    pool.getConnection(function(err, conn){
                 if(err){
                     err.code = 500;
                     return next(err);
                    }

                    var sql = 'insert into boys set ?';
                   
                    var info = {
                        id : id,
                        name : name,
                        age: age
                    };

            conn.query(sql, info, function(err, results){
                       
                    if(err){
                     err.code = 500;
                     return next(err);
                    }


                    var sql2 = 'select * from boys where boys.id = ?';
                    conn.query(sql2, [id], function(err, results){
                              if(err){
                            err.code = 500;
                            return next(err);
                            }

                            var boylist = {
                                count : results.length,
                                data: results
                            }
                             res.send(boylist);
                             conn.release();
                    });
                    
            });
            console.log(addboys);
    });
}




module.exports = router;
