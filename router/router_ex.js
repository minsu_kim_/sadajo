var ejs = require('ejs');
var fs = require('fs');
var mysql = require('mysql');
var express = require('express');
var async = require('async');
var model = require('../model/model');

var router = express.Router();


router.route('/home/')
	.post(getHome);

router.route('/registerSchedule/')
	.post(registerSchedule);

router.route('/shoplist/')
	.post(getShoplist);

router.route('/likelist/')
	.post(getLikelist);

router.route('/request/')
	.post(request);

router.route('/chat/')
	.post(chat);



function chat(req, res, next) {console.log('[router] chat in!');

	//post 방식
	var body = req.body;
	var type = body.type;
	var user = body.user;
	var carr = body.carr || 0;
	var room = body.room || 0;
	var img = 'partner_img';
	

	// 임시로 GET 방식
	// var type = req.query.type;
	// var user = req.query.user;	 
	// var carr = req.query.carr || 0;
	// var room = req.query.room;

	console.log('[router] chat type : ', type, ' user : ',user, ' room : ', room, ' img : ',img);
	if(!type||!user) {
		console.log('>>> type or user is null');
		return false;
	}
	//room number 설정
	//대화하기 버튼 클릭시
	if(type == 'new') {console.log('>>> type new');
         //room number 받아옴  
         model.getRoom( (err, result) => {
            
         	if(err) {
			console.log('>>> model.insertRequest error!');
			return next(err);
			}
			console.log('[router] room : ', result.room);
            
            room = result.room; //기존의 마지막 채팅방 번호 가져옴
            room++; // +1
            console.log('[router] room+1 : ', room);

            //응답 data 생성
            var data = {room : room, req : user, carr : carr};
            console.log('[router] mid data : ', data);
            model.insertRoom(data, (err, result) => {

				if(err) {
					console.log('>>> model.insertRoom error!');
					return next(err);
				}

				console.log('>>> model.insertRoom success!');

				//파트너 이미지 가져옴
				// model.getUserimg(carr, (err, result) => {



				// });
				chatdata = {room : room, user : user, partner : carr, img : img};
				console.log('[router] chat data : ', chatdata);
				res.status(200).send(chatdata);
			});
            
            

         });
         
    }else{console.log('>>> type join');

    	//파트너 이미지 가져옴
		// model.getUserimg(carr, (err, result) => {



		// });
    	var chatdata = {
    			room : room,
				user : user,
				partner : carr,
				img	 : img			
		};

		console.log('[router] chat date : ', chatdata);

		res.status(200).send(chatdata);

    }
        
}

function request(req, res, next) {console.log('>>> [router] request in');
	
	var body = req.body;
	var data = { req : body.req, carr : body.carr, country : body.country, goods : body.goods, price : body.price, detail : body.detail };
		
	model.insertRequest(data, (err, result) => {

		if(err) {
			console.log('>>> model.insertRequest error!');
			return next(err);
		}

		console.log('>>> model.insertRequest success!');
		res.status(200).send(result);
	});
}


function registerSchedule(req, res, next) {console.log('>>> [router] registerSchedule in');
	
	var body = req.body;
	var data = { user : body.user, country : body.country, city : body.city, start : body.start, end : body.end };
		
	model.insertSchedule(data, (err, result) => {

		if(err) {
			console.log('>>> model.insertSchedule error!');
			return next(err);
		}

		console.log('>>> model.insertSchedule success!');
		res.status(200).send(result);
	});
}


function getShoplist(req, res, next) {console.log('>>> [router] getShoplist in');	

	var body = req.body;
	var id = body.user;console.log('>>> [router] id : ', id);

	model.getShoplist(id, (err, result) => {


		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var shoplist = {
				msg : 'success',
				list : result				
			};

		console.log('>>> [router] shoplist : ', shoplist);  
		res.status(200).send(shoplist);
	});
}


function getLikelist(req, res, next) {console.log('>>> [router] getLikelist in');	

	var body = req.body;
	var id = body.user;console.log('>>> [router] id : ', id);

	model.getLikelist(id, (err, result) => {


		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var likelist = {
				msg : 'success',
				list : result				
			};

		console.log('>>> [router] likelist : ', likelist);  
		res.status(200).send(likelist);
	});
}


function getHome (req, res, next) {console.log('>>> [router] getHome in');	

	var body = req.body;console.log('>>> [router] getHome 1');	
	var id = body.user || 1;console.log('>>> [router] getHome id : ', id);
	var num = 0;console.log('>>> [router] getHome 2');	
	var title_country = 'KOREA';
	var travel_country = '미국';
	var country_code = 'USA';
	var country_img = 'usa_img';
	var count = 0;
	var end_date = '';
	var start_date = '';
	var start = '';
	var end = '';

	async.series([
			//여행일정 가져옴
			function travelSchedule(callback) {				
				//여행일정이 있는지 확인
				model.getScheduleCount(id, (err, result) => {

					if(err) {
						console.log('>>> [router] model.getSchedule error!');
						return next(err);
					}

					count = result;
					console.log('>>> [router] getHome model.getScheduleCount count : ', count);

					//여행일정이 있다면
					if(count>0) {console.log('>>> [router] getHome [if] count > 0');

						//가장 늦은 귀국일을 가져옴
						model.getLastSchedule(id, (err, result) => {

							if(err) {
								console.log('>>> [router] model.getLastSchedule error!');
								return next(err);
							}
							
							console.log('>>> [router] model.getLastSchedule success! end_date : ', result[0].end_date);

							//귀국일이 이미 지났는지 확인
							var theDate = result[0].end_date || '';
							//var theDate = new Date();
							var today = new Date();

							//지나지 않았다면 여행국가, 시작일, 종료일 변경
							if (theDate.getTime() > today.getTime()) {console.log('>>> [router] model.getLastSchedule [if] getTime() > 0');
								
								console.log('>>> schedule exists');
								title_country = result[0].country_name_eng;
								travel_country = result[0].country_name;
								country_code = result[0].country_code;
								country_img = result[0].country_img;
								end_date = result[0].end_date;
								start_date = result[0].start_date;								
								start = start_date.toISOString().substring(0, 10);
								end = end_date.toISOString().substring(0, 10);
								var travel = {
										title_country : title_country,
										start_date : start,
										end_date : end
								};
								console.log('>>> [router] model.getLastSchedule [json] travel : ', travel);
								callback(null, [travel, travel_country, country_img]);
								
								
							//지났다면 유지
							}else {
								console.log('>>> [router] model.getLastSchedule [if] getTime() <= 0');
								var travel = {
										title_country : title_country,
										start_date : start_date,
										end_date : end_date
								};
								console.log('>>> [router] model.getLastSchedule [json] travel : ', travel);
								callback(null, [travel, travel_country, country_img]);
								
							}				

							
								
						});

						
					
					}
					//여행일정이 없다면
					else{
						console.log('>>> [router] model.getLastSchedule [if] count = 0');									
						var travel = {
										title_country : title_country,
										start_date : start_date,
										end_date : end_date
								};
						console.log('>>> [router] model.getLastSchedule [json] travel : ', travel);
						callback(null, [travel, travel_country, country_img]);
						
					}

												
				});
			},


			//해시태그 가져옴
			function hashtag(callback) {console.log('>>> [router] hashtag in!');
				
				model.getHashtag(country_code, (err, result) => {
					if(err) {
						console.log('>>> model.getHashtag error!');
						return next(err);
					}

					console.log('>>> [router] hashtag : ',result);
					console.log('>>> [router] hashtag num : ',result.length);
					var tag = [];
					for(var i=0; i<result.length; i++)
					{
						tag.push(result[i].tag);
					}
					console.log('>>> [router] hashtag array : ',tag);
					callback(null, tag);
				});
			},


			//쇼핑리스트 가져옴
			function shoplist(callback) {
							
				model.getShoplistCountry(country_code, (err,result) => {
					if(err) {
						console.log('>>> [router] model.getShoplistCountry error!');
						return next(err);
					}

					console.log('>>> [router] model.getShoplistCountry success!');
					callback(null, result);
				});
			}
		],


		function(err, results) {
			if (err) {
				console.error('에러 : ', err.message);
         		return;
			}

			console.log('>>> [router] model.getTravel success!');
			var home = {
				msg : 'success',
				travel : results[0][0],
				travel_country : results[0][1],
				country_img : results[0][2],
				tag : results[1],
				shoplist : results[2]
			};

			console.log('>>> [router] model.getTravel Total data : ', home);  
			res.status(200).send(home);

		}
	);
}




module.exports = router;