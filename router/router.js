var ejs = require('ejs');
var fs = require('fs');
var mysql = require('mysql');
var express = require('express');
var async = require('async');
var model = require('../model/model');
var mongo = require('../model/mongo');
var request1 = require('request');

var router = express.Router();


//홈
router.route('/home/')
	.post(getHome);

//여행일정 등록
router.route('/registerSchedule/')
	.post(registerSchedule);

//쇼핑리스트 내역
router.route('/shoplist/')
	.post(getShoplist);

//쇼핑리스트 상세
router.route('/shopdetail')
	.post(shopdetail);	

//찜리스트 목록
router.route('/likelist/')
	.post(getLikelist);

//찜리스트 상세
router.route('/likedetail')
	.post(likedetail);	

//사다조 요청
router.route('/request/')
	.post(request);

//채팅방 입장
router.route('/chat/')
	.post(chat);

//채팅방 리스트
router.route('/chatlist')
	.post(chatlist);

//찜리스트 담기
router.route('/like/')
	.post(like);

//쇼핑리스트 담기
router.route('/shop/')
	.post(shop);

//마이페이지 가기
router.route('/userpage')
	.post(userpage);

//사다조 목록
router.route('/reqlist')
	.post(reqlist);

//사다줌 목록
router.route('/carrlist')
	.post(carrlist);

//마이페이지 _ 후기
router.route('/review')
	.post(review);

//마이페이지 _ 팁
router.route('/mytip')
	.post(mytip);

//마이페이지 _ 사다조 요청 수락
router.route('/sayYes')
	.post(sayYes);



//사다조 요청 수락
function sayYes(req, res, next) {console.log('>>> [router] sayYes in');

	var body = req.body;
	var code = body.code;

	//data 가져옴
	model.sayYes(code, (err, result) => {

		if(err) {
			console.log('>>> [router] model.sayYes error!');
			res.status(200).send({status : 0});
		}

		console.log('>>> [router] model.sayYes success!');
		console.log('>>> [router] result : ', {status : 1});
		res.status(200).send({status : 1});

	});

}


//마이페이지 _ 팁
function mytip(req, res, next) {console.log('>>> [router] mytip in');

	var body = req.body;
	var owner = body.owner;
	var user = body.user;

	//data 가져옴
	model.getMytip(owner, (err, result) => {

		if(err) {
			console.log('>>> [router] model.getMytip error!');
			return next(err);
		}

		console.log('>>> [router] model.getMytip success!');
		console.log('>>> [router] result : ', result);
		res.status(200).send(result);

	});

}


//마이페이지 _ 후기
function review(req, res, next) {console.log('>>> [router] review in');

	var body = req.body;
	var owner = body.owner;
	var user = body.user;

	//data 가져옴
	model.getReview(owner, (err, result) => {

		if(err) {
			console.log('>>> [router] model.getReview error!');
			return next(err);
		}

		console.log('>>> [router] model.getReview success!');
		console.log('>>> [router] result : ', result);
		res.status(200).send(result);

	});

}

//거래내역 (사다줌)
function carrlist(req, res, next) {console.log('>>> [router] carrlist in');

	var body = req.body;
	var owner = body.owner;console.log('>>> [router] carrlist owner : ', owner);
	var user = body.user;

	//data 가져옴
	model.getCarrlist(owner, (err, result) => {

		if(err) {
			console.log('>>> [router] model.getCarrlist error!');
			return next(err);
		}

		console.log('>>> [router] model.getCarrlist success!');
		console.log('>>> [router] result : ', result);
		res.status(200).send(result);

	});

}


//거래내역 (사다조)
function reqlist(req, res, next) {console.log('>>> [router] reqlist in');

	var body = req.body;
	var owner = body.owner;console.log('>>> [router] reqlist owner : ', owner);
	var user = body.user;

	//data 가져옴
	model.getReqlist(owner, (err, result) => {

		if(err) {
			console.log('>>> [router] model.getReqlist error!');
			return next(err);
		}

		console.log('>>> [router] model.getReqlist success!');
		console.log('>>> [router] result : ', result);
		res.status(200).send(result);

	});

}


//마이페이지
function userpage(req, res, next) {console.log('>>> [router] userpage in');

	var body = req.body;
	var owner = body.owner;
	var user = body.user;

	console.log('>>> [test] user : ', user, ' owner : ', owner);

	//data 가져옴
	model.getUserpage(owner, (err, result) => {

		if(err) {
			console.log('>>> [router] model.checkLikelist error!');
			return next(err);
		}

		console.log('>>> [router] model.getUserpage success!');
		console.log('>>> [router] result : ', result);
		res.status(200).send(result);

	});


}


//쇼핑리스트 담기
function shop(req, res, next) {console.log('>>> [router] shop in');
	
	var body = req.body;
	var user = body.user || 77;
	var country = body.goods.substring(0,3) || 'KOR';
	var goods = body.goods || 77;//goods_code
	var list = '';
	var data = {user : user, goods : goods, country : country, list : list};


	async.series([
		//리스트 코드 확인
		function checklist (callback) {

			//쇼핑리스트가 있는지 확인
			model.checkShoplist(data, (err, result) => {

				if(err) {
						console.log('>>> [router] model.checkShoplist error!');
						return next(err);
					}

				console.log('>>> [router] model.checkShoplist success!');
				console.log('>>> [router] result : ', result);
				var count =  result[0].count;
				
				//리스트가 없으면 응답				
				if(!count){console.log('>>> [router] checklist no list!');

					//응답
					var shop_result = {
						listcode : 'no list',
						insertResult : 'failed'						
					};
					console.log('>>> [router] checklist no list result : ', shop_result);
					res.status(200).send(shop_result);

					//리스트가 있으면 리스트 코드 확인
				}else{console.log('>>> [router] checklist list exists!');

					list = result[0].list_code || 7777;

					//리스트에 상품이 있는지 확인
					model.checkGoods2(list, goods, (err, result2) => {console.log('>>> [router] checkGoods2 success!');

						if(err) {
							console.log('>>> [router] model.checkGoods2 error!');
							return next(err);
						}

						console.log('>>> [router] checkGoods2 result : ',result2);
						//있으면 like_result 응답
						if(result2.count){
							var shop_result = {
								type : 'failed'								
								};
							res.status(200).send(shop_result);
						}
						//없으면 리스트 코드 담아 콜백
						else{							
							data.list = list;console.log('>>> [router] checklist  list_code : ', data.list );
							callback(null, list);
						}
					});
					
				}
			});
		},

		//리스트에 담기
		function goodsonlist (callback) {console.log('>>> [router] goodsonlist in!');

			//상품 담기
			model.insertShopGoods(data, (err, result) => {

				if(err) {
					console.log('>>> [router] model.insertShopGoods error!');
					return next(err);
				}

				console.log('>>> [router] model.insertShopGoods success!');
				
				//쇼핑리스트 사진 업데이트
				model.updateShoplistImg(data, (err,result1) => {

					if(err) {
					console.log('>>> [router] model.updateShoplistImg error!');
					return next(err);
					}

					console.log('>>> [router] model.updateShoplistImg success!');
					
					//쇼핑리스트 상품개수 업댓
					model.updateShopGoodsNum(data, (err,result2) => {

						if(err) {
						console.log('>>> [router] model.updateShopGoodsNum error!');
						return next(err);
						}

						console.log('>>> [router] model.updateShopGoodsNum success!');
						
						//상품 쇼핑리스트 담음 횟수 업댓						
						model.updateShopNum(data.goods, (err, result3) => {console.log('>>> [router] updateShopNum result : ', result3);

							if(err) {
							console.log('>>> [router] model.updateShopNum error!');
							return next(err);
							}

							console.log('>>> [router] model.updateShopNum success!');
							callback(null, 'success');
						});
						
					});

				});

			});
		},

		function getShopCount (callback) {console.log('>>> [router] getShopCount in!');

			//찜 횟수 가져옴
			model.getShopCount(data, (err,result) => {

				if(err) {
					console.log('>>> [router] model.getShopCount error!');
					return next(err);
				}

				console.log('>>> [router] model.getShopCount success!');
				callback(null, result);

					
			})
		}

		], 

		function(err, results) {

			var shop_result = {
				type : 'ok',
				listcode : results[0],
				insertResult : results[1],
				shopCount : results[2][0].shop_num
			};
			res.status(200).send(shop_result);
		}
	);
	
}


//찜하기
function like(req, res, next) {console.log('>>> [router] like in');
	
	var body = req.body;
	var user = body.user || 77;
	var country = body.goods.substring(0,3) || 'KOR';
	var goods = body.goods || 77;//goods_code
	var list = '';
	var data = {user : user, goods : goods, country : country, list : list};


	async.series([
		//리스트 코드 확인
		function checklist (callback) {

			//찜리스트가 있는지 확인
			model.checkLikelist(data, (err, result) => {

				if(err) {
						console.log('>>> [router] model.checkLikelist error!');
						return next(err);
					}

				console.log('>>> [router] model.checkLikelist success!');
				console.log('>>> [router] result : ', result);
				var count =  result[0].count;
				
				//리스트가 없으면 리스트 생성				
				if(!count){console.log('>>> [router] checklist no list!');

					//찜리스트 생성
					model.insertLikelist(data, (err, result1) => {

						if(err) {
							console.log('>>> [router] model.insertLikelist error!');
							return next(err);
						}

						console.log('>>> [router] model.insertLikelist success!');
						console.log('>>> [router] result1 : ', result1);
						list = result1.insertId;
						data.list = list;console.log('>>> [router] checklist  list_code : ', data.list );
						callback(null, list);
					});

					//리스트가 있으면 리스트 코드 확인
				}else{console.log('>>> [router] checklist list exists!');

					list = result[0].list_code || 7777;
					//리스트에 상품이 있는지 확인
					model.checkGoods(list, goods, (err, result2) => {console.log('>>> [router] checkGoods success!');

						if(err) {
							console.log('>>> [router] model.checkGoods error!');
							return next(err);
						}

						console.log('>>> [router] checkGoods result : ',result2);
						//있으면 like_result 응답
						if(result2.count){
							var like_result = {
								type : 'failed'								
								};
							res.status(200).send(like_result);
						}
						//없으면 리스트 코드 담아 콜백
						else{							
							data.list = list;console.log('>>> [router] checklist  list_code : ', data.list );
							callback(null, list);
						}
					});
				}
			});
		},

		//리스트에 담기
		function goodsonlist (callback) {console.log('>>> [router] goodsonlist in!');

			//상품 담기
			model.insertGoods(data, (err, result) => {

				if(err) {
					console.log('>>> [router] model.insertGoods error!');
					return next(err);
				}

				console.log('>>> [router] model.insertGoods success!');
				
				//찜리스트 사진 업데이트
				model.updateLikelistImg(data, (err,result1) => {

					if(err) {
					console.log('>>> [router] model.updateLikelistImg error!');
					return next(err);
					}

					console.log('>>> [router] model.updateLikelistImg success!');
					
					//찜리스트 상품개수 업댓
					model.updateLikeGoodsNum(data, (err,result2) => {

						if(err) {
						console.log('>>> [router] model.updateLikeGoodsNum error!');
						return next(err);
						}

						console.log('>>> [router] model.updateLikeGoodsNum success!');
						
						//상품 찜 횟수 업댓						
						model.updateLikeNum(data.goods, (err, result3) => {console.log('>>> [router] updateLikeNum result : ', result3);

							if(err) {
							console.log('>>> [router] model.updateLikeNum error!');
							return next(err);
							}

							console.log('>>> [router] model.updateLikeNum success!');
							callback(null, 'success');
						});
						
					});

				});

			});
		},

		function getLikeCount (callback) {console.log('>>> [router] getLikeCount in!');

			//찜 횟수 가져옴
			model.getLikeCount(data, (err,result) => {

				if(err) {
					console.log('>>> [router] model.getLikeCount error!');
					return next(err);
				}

				console.log('>>> [router] model.getLikeCount success!');
				callback(null, result);

					
			})
		}

		], 

		function(err, results) {

			var like_result = {
				type : 'ok',
				listcode : results[0],
				insertResult : results[1],
				likeCount : results[2][0].like_num
			};
			res.status(200).send(like_result);
		}
	);
	
}


function shopdetail(req, res, next) {console.log('[router] shopdetail in!');
	
	// listcode 가져오기
	var body = req.body;
	var code = body.listcode; console.log('[router] shopdetail code : ', code);
	var user = body.user;

	//data 가져오기 : user, 상품코드, 상품명, 상품이미지
	model.getshopdetail(code, (err,result) => {
		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var detail = {
				msg : 'success',
				user : user,
				list : result				
			};

		console.log('>>> [router] shopdetail : ', detail);  
		res.status(200).send(detail);

	});
}


function likedetail(req, res, next) {console.log('[router] likedetail in!');
	
	// listcode 가져오기
	var body = req.body;
	var code = body.listcode; console.log('[router] likedetail code : ', code);
	var user = body.user;

	//data 가져오기 : user, 상품코드, 상품명, 상품이미지
	model.getlikedetail(code, (err,result) => {
		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var detail = {
				msg : 'success',
				user : user,
				list : result				
			};

		console.log('>>> [router] likedetail : ', detail);  
		res.status(200).send(detail);

	});
}


function chatlist(req, res, next) {console.log('[router] chatlist in!');

	//user 가져오기
	var body = req.body;
	var user = body.user;console.log('[router] chatlist user : ', user);

	//data 가져오기 : req, carr, room, req_img, carr_img, req_nick, carr_nick, msg, lastdate5
	model.getChatlist(user, (err, result) => {

		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var chatlist = {
				msg : 'success',
				user : user,
				list : result				
			};

		console.log('>>> [router] chatlist : ', chatlist);  
		res.status(200).send(chatlist);

	});
	
}


function chat(req, res, next) {console.log('[router] chat in!');

	//post 방식
	var body = req.body;
	var type = body.type;
	var user = body.user;
	var carr = body.carr || 0;
	var room = body.room || 0;
	var img = 'partner_img';
	var nick = '';
	var key = 'AIzaSyBSePZYgz5o1_sVqQbpMfrDz9fmFp5X9P4';
	var token = '';
	

	console.log('[router] chat type : ', type, ' user : ',user, ' room : ', room, ' img : ',img);
	if(!type||!user) {
		console.log('>>> type or user is null');
		return false;
	}
	//room number 설정
	//대화하기 버튼 클릭시
	if(type == 'new') {console.log('>>> type new');
         
			async.series([
				//room number 받아옴  
				function getroom(callback) {console.log('>>> [router] getroom in!' );
					//room number 받아옴  
			        model.getRoom( (err, result) => {
			            
			         	if(err) {
						console.log('>>> model.getRoom error!');
						return next(err);
						}
						console.log('>>> [router] room : ', result.room);
			            
			            room = result.room; //기존의 마지막 채팅방 번호 가져옴
			            room++; // +1
			            console.log('>>> [router] room+1 : ', room);
			            callback(null, room);
			        });

				},
				//push token 받아옴
				function getToken(callback) {console.log('>>> [router] getToken in! carr : ', carr);

					model.getToken(carr, (err, result) => {

						if(err) {
						console.log('>>> model.getToken error!');
						return next(err);
						}
						console.log('>>> [router] token : ', result);
			            
			            token = result.token || 'no data'; // +1
			            console.log('>>> [router] token : ', token);
			            callback(null, result);
					});
				},
				//push 보냄
				function shootpush(callback) {console.log('>>> [router] shootpush in! room : ', room, ' token : ', token);
					
					//요청자 정보 가져옴
					model.getUserInfo(user, (err, result) => {console.log('>>> [router] insertRoom user : ', user);

						if(err) {
						console.log('>>> model.getUserInfo error!');
						return next(err);
						}

						console.log('[router] getUserInfo result : ', result[0]);
						var user_img = result[0].img || '';
						var user_nick = result[0].nick || '';
						//푸시 메세지 전송
			            request1({
							url:'https://fcm.googleapis.com/fcm/send',
							method:'POST',
							headers:{
								'Content-Type':'application/json',
								'Authorization':'key=' + key
							},
							body:JSON.stringify({
								"msg":"success",
								"data":{
									"message" :'success',
									"room" : room,
									"partner" : user,
									"nick" : user_nick,
									"img" : user_img
								},
								"to": token
							})
							},function(err,response,body){console.error('>>> in!');
								if(err){
									console.error(error,response,body);
									callback(null, 'failed');
								}else if(response.statusCode>=400){
									console.error('HTTP Error:'+response.statusCode+'-'+response.statusMessage+'\n'+body);
									callback(null, 'failed');
								}else{
									console.log('Done');
									callback(null, 'success');
								}
						});

					});

				},

				//db에 document 생성
				function makeRoom(callback) {console.log('>>> [router] makeRoom in!');

					mongo.createRoom(room, (err, result7) => {console.log('>>>');

						if(err) {
						console.log('>>> [router] mongo.createRoom error!');
						return next(err);
						}
						
						console.log('>>> [router] mongo.createRoom success!');
						console.log('>>> [router] mongo.createRoom result : ', result7);
						callback(null, 'mongo success');

					});

				}

				],
				//응답
				function (err, results) {console.log('>>> [router] last function in! room : ',room,' req : ',user, ' carr : ',carr);
					
					//응답 data 생성
		            var data = {room : room, req : user, carr : carr};
		            console.log('[router] mid data : ', data);
		            model.insertRoom(data, (err, result) => {console.log('>>> [router] insertRoom in!');

						if(err) {
							console.log('>>> [router] model.insertRoom error!');
							return next(err);
						}

						console.log('>>> [router] model.insertRoom success!');

						//파트너 정보 가져옴
						model.getUserInfo(carr, (err, result) => {console.log('>>> [router] insertRoom carr : ', carr);

							if(err) {
							console.log('>>> model.getUserInfo error!');
							return next(err);
							}

							console.log('[router] getUserInfo result : ', result[0]);
							img = result[0].img || '';
							nick = result[0].nick || '';
							var chatdata = {room : room, user : user, partner : carr, nick : nick, img : img};
							console.log('[router] chat data : ', chatdata);
							res.status(200).send(chatdata);

						});
						
					});
				}
				)
              

    }else{console.log('>>> type join');

    	var partner = '';

    	async.series([

    		//파트너 user_code 가져옴
    		function partnerCode(callback) {	
    	
		    	model.getPartner(room, (err, result) => {

		    		if(err) {
							console.log('>>> model.getPartner error!');
							return next(err);
					}

					console.log('>>> [router] partner_code : ', result);
					var req = result[0].req || '';
					var carr = result[0].carr || '';console.log('>>> [router] req : ', req);
					if(user == req) {console.log('>>> [router] if user == req');
						partner = carr;
					}else{console.log('>>> [router] if user != req');
						partner = req;
						console.log('>>> [router] if user != req partner : ', partner);
					}
					console.log('>>> [router] partnerCode  partner : ', partner);
					callback(null, result);

				});

			},

			//파트너 정보 가져옴
			function partnerInfo(callback) {console.log('>>> [router] partnerInfo in! partner : ', partner);

				model.getUserInfo(partner, (err, result) => {

		    		if(err) {
							console.log('>>> model.getUserInfo error!');
							return next(err);
					}

					console.log('>>> [router] model.getUserInfo success! result : ', result);
					callback(null, result);
				});

			}
		],
		

    	function(err, results) {
			
			if (err) {
				console.error('에러 : ', err.message);
         		return;
			}

			console.log('>>> [router] async Total data : ', results);  
			img = results[1][0].img || '';
			console.log('[router] partner_img : ', img);
			nick = results[1][0].nick || '';
			console.log('[router] partner_nick : ', nick);
			var chatdata = {room : room, user : user, partner : partner, nick : nick, img : img};
			console.log('[router] chat data : ', chatdata);
			res.status(200).send(chatdata);

		}	

		);

    }	
        
}


function request(req, res, next) {console.log('>>> [router] request in');
	
	var body = req.body;
	var data = { req : body.req, carr : body.carr, country : body.country, goods : body.goods, price : body.price, detail : body.detail };
		
	model.insertRequest(data, (err, result) => {

		if(err) {
			console.log('>>> model.insertRequest error!');
			return next(err);
		}

		console.log('>>> model.insertRequest success!');
		res.status(200).send(result);
	});
}


function registerSchedule(req, res, next) {console.log('>>> [router] registerSchedule in');
	
	var body = req.body;
	var data = { user : body.user, country : body.country, city : body.city, start : body.start, end : body.end };
		
	model.insertSchedule(data, (err, result) => {

		if(err) {
			console.log('>>> model.insertSchedule error!');
			return next(err);
		}

		console.log('>>> model.insertSchedule success!');
		res.status(200).send(result);
	});
}


function getShoplist(req, res, next) {console.log('>>> [router] getShoplist in');	

	var body = req.body;
	var id = body.user;console.log('>>> [router] id : ', id);

	model.getShoplist(id, (err, result) => {


		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var shoplist = {
				msg : 'success',
				user : id,
				list : result				
			};

		console.log('>>> [router] shoplist : ', shoplist);  
		res.status(200).send(shoplist);
	});
}


function getLikelist(req, res, next) {console.log('>>> [router] getLikelist in');	

	var body = req.body;
	var id = body.user;console.log('>>> [router] id : ', id);

	model.getLikelist(id, (err, result) => {


		if ( err ){
            return next(err);
        }
        console.log('>>> data got : ', result);
        var likelist = {
				msg : 'success',
				user : id,
				list : result				
			};

		console.log('>>> [router] likelist : ', likelist);  
		res.status(200).send(likelist);
	});
}


function getHome (req, res, next) {console.log('>>> [router] getHome in');	

	var body = req.body;console.log('>>> [router] getHome 1');	
	var id = body.user || 1;console.log('>>> [router] getHome id : ', id);
	var token = body.token;console.log('>>> [router] getHome token : ', token);
	var num = 0;console.log('>>> [router] getHome 2');	
	var title_country = 'KOREA';
	var travel_country = '미국';
	var country_code = 'USA';
	var country_img = 'https://s3.ap-northeast-2.amazonaws.com/sadajobox/flag/usa.png';
	var count = 0;
	var end_date = '';
	var start_date = '';
	var start = '';
	var end = '';

	async.series([
			//여행일정 가져옴
			function travelSchedule(callback) {				
				//여행일정이 있는지 확인
				model.getScheduleCount(id, (err, result) => {

					if(err) {
						console.log('>>> [router] model.getSchedule error!');
						return next(err);
					}

					count = result;
					console.log('>>> [router] getHome model.getScheduleCount count : ', count);

					//여행일정이 있다면
					if(count>0) {console.log('>>> [router] getHome [if] count > 0');

						//가장 늦은 귀국일을 가져옴
						model.getLastSchedule(id, (err, result) => {

							if(err) {
								console.log('>>> [router] model.getLastSchedule error!');
								return next(err);
							}
							
							console.log('>>> [router] model.getLastSchedule success! end_date : ', result[0].end_date);

							//귀국일이 이미 지났는지 확인
							var theDate = result[0].end_date || '';
							//var theDate = new Date();
							var today = new Date();

							//지나지 않았다면 여행국가, 시작일, 종료일 변경
							if (theDate.getTime() > today.getTime()) {console.log('>>> [router] model.getLastSchedule [if] getTime() > 0');
								
								console.log('>>> schedule exists');
								title_country = result[0].country_name_eng;
								travel_country = result[0].country_name;
								country_code = result[0].country_code;
								country_img = result[0].country_img;
								end_date = result[0].end_date;
								start_date = result[0].start_date;								
								start = start_date.toISOString().substring(0, 10);
								end = end_date.toISOString().substring(0, 10);
								var travel = {
										title_country : title_country,
										start_date : start,
										end_date : end
								};
								console.log('>>> [router] model.getLastSchedule [json] travel : ', travel);
								callback(null, [travel, travel_country, country_img]);
								
								
							//지났다면 유지
							}else {
								console.log('>>> [router] model.getLastSchedule [if] getTime() <= 0');
								var travel = {
										title_country : title_country,
										start_date : start_date,
										end_date : end_date
								};
								console.log('>>> [router] model.getLastSchedule [json] travel : ', travel);
								callback(null, [travel, travel_country, country_img]);
								
							}				

							
								
						});

						
					
					}
					//여행일정이 없다면
					else{
						console.log('>>> [router] model.getLastSchedule [if] count = 0');									
						var travel = {
										title_country : title_country,
										start_date : start_date,
										end_date : end_date
								};
						console.log('>>> [router] model.getLastSchedule [json] travel : ', travel);
						callback(null, [travel, travel_country, country_img]);
						
					}

												
				});
			},


			//해시태그 가져옴
			function hashtag(callback) {console.log('>>> [router] hashtag in!');
				
				model.getHashtag(country_code, (err, result) => {
					if(err) {
						console.log('>>> model.getHashtag error!');
						return next(err);
					}

					console.log('>>> [router] hashtag : ',result);
					console.log('>>> [router] hashtag num : ',result.length);
					var tag = [];
					for(var i=0; i<result.length; i++)
					{
						tag.push(result[i].tag);
					}
					console.log('>>> [router] hashtag array : ',tag);
					callback(null, tag);
				});
			},


			//쇼핑리스트 가져옴
			function shoplist(callback) {
							
				model.getShoplistCountry(country_code, (err,result) => {
					if(err) {
						console.log('>>> [router] model.getShoplistCountry error!');
						return next(err);
					}

					console.log('>>> [router] model.getShoplistCountry success! result : ', result);
					callback(null, result);
				});
			}
		],


		function(err, results) {
			if (err) {
				console.error('에러 : ', err.message);
         		return;
			}

			console.log('>>> [router] model.getTravel success!');
			var home = {
				msg : 'success',
				travel : results[0][0],
				travel_country : results[0][1],
				country_img : results[0][2],
				tag : results[1],
				shoplist : results[2]
			};
			//token이 있으면 저장
			if(token){console.log('>>> [router] token exist!');
				model.saveToken(id, token, (err,result) => {
					if(err) {
						console.log('>>> [router] model.saveToken error!');
					}else{
						console.log('>>> [router] model.saveToken success!');
					}
					console.log('>>> [router] model.getTravel Total data : ', home);  
					res.status(200).send(home);

				});
			}else{console.log('>>> [router] no token!');
				console.log('>>> [router] model.getTravel Total data : ', home);  
				res.status(200).send(home);
			}			

		}
	);
}




module.exports = router;