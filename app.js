var express = require('express');
var router = express.Router();
var nconfig = require('nconfig');
var http = require('http');
var app = express();
var server = http.createServer(app);
var morgan = require('morgan');
var bodyParser = require("body-parser");


var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var flash = require('connect-flash');


app.use(morgan('dev'));

app.use(router);

//bodyParser
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());


app.use(passport.initialize());


app.use(require('./router/router'));
const chat = require('./service/chatService');


app.use(require('./boyslist'));
app.use(require('./goodsRouter'));
app.use(handleError);
app.set('view engine', 'ejs');




// serialize

// 인증후 사용자 정보를 세션에 저장

passport.serializeUser(function(user, done) {

    console.log('serialize');

    done(null, user);

});

 


// deserialize

// 인증후, 사용자 정보를 세션에서 읽어서 request.user에 저장

passport.deserializeUser(function(user, done) {

    //findById(id, function (err, user) {

    console.log('deserialize');

    done(null, user);

    //});

});

/****************************************************** */
//웹으로 할때만 이용
// passport.use(new FacebookStrategy({

//         clientID: ' 1248737848535401 ',

//         clientSecret: 'd6a1561b354c6d794c5c5c0848958b05',

//         callbackURL: '/auth/facebook/callback'

//     },

//     function(accessToken, refreshToken, profile, done) {

//        process.nextTick(function(){
// 		   console.log(profile);
// 		   user.findOne({'user_code':profile.id}, function(err, user){
// 			   if(err){
// 				   return done(err);
// 			   }
// 			   if(user){
// 				   return done(null, user);
// 			   }else{
// 				   var newUser = newUser();

// 				   newUser.fb.id = profile.id;
// 				   newUser.fb.accessToken = accessToken;
// 				   newUser.fb.email = profile.emails[0].value;

// 				   newUser.save(function(err){
// 					   if(err){
// 						   throw err;
// 					   }
// 					   return done(null, newUser);
// 				   });
// 			   }


// 		   });
// 	   });

//     }

// ));
/****************************************************** */

var FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
    clientID: '1248737848535401',
    clientSecret: 'd6a1561b354c6d794c5c5c0848958b05'
  }, function(accessToken, refreshToken, profile, done) {
   console.log(profile);
   done(null, profile);
	
  }
));


server.listen(3000, function(){
	console.log("Connected & Listen to port 3000");
});











function handleError(err, req, res, next){
 	
 	if(err.code)

 		res.status(err.code);
 	else
 		res.status(500);

	res.send({msg:err.message});
};

chat(server);
