

module.exports = function(server) {console.log('>>> [svc] connentSocket in');

	var io = require('socket.io')(server);
	const mongo = require('../model/mongo');

	// 클라이언트가 접속할 때마다 별도의 인스턴스(커넥션) 생성 
	io.on('connection', function(socket){
	      
	    console.log('>>> [svc] connected');
	     
	     // 기본값은 livingroom
	    var room;
	    //room = 'livingroom';
	    //socket.join(room);
	    console.log('>>> connected room : ', room);

	    // 개별 클라이언트에 환영 메세지 ( socket.emit : 해당 클라이언트에게 전송 )
	    socket.emit('toClient', {sender:'Admin', msg:'connected'});
	    
	     // 채팅 메세지는 채팅방의 모든 클라이언트에게 전송
	    socket.on('toServer', function(data) {
	        var msg = data['msg'];
	        var sender = data['sender'];	        
	        var chat = {sender:sender, msg:msg};
	        
	        // 메세지 DB insert
	        mongo.saveMsg(room, sender, msg, (err, result5) => {console.log('>>> [svc] saveMsg in');

	        	if(err) {
	        		console.log('>>> [svc] model.saveMsg error!');
					return next(err);
	        	}
	        	console.log('>>> [router] mongo.saveMsg success!');
				console.log('>>> [router] mongo.saveMsg result : ', result5);
				chat.date = result5.date;
				chat.time = result5.time;
				io.to(room).emit('toClient', chat);
	        	console.log('>>> [' + room + '] ' + sender + ' >> ' + msg); 
	        	console.log('>>> [' + room + '] ' + chat);

	        });

	        // 채팅방으로 메세지 이벤트 발생 ( io.to(room).emit : 채팅방에 있는 모든 클라이언트에게 전송)       
	        
	    });
	     
	    // 채팅방 입장하기
	    socket.on('joinRoom', function(data) {
	        
	        console.log('>>> join room : ', room);
	        room = data.room;
	        var user = data.user;
	        socket.join(room);
	        //var nick = users[socket.id];    
	        console.log(user + ' join ' + room);

	        mongo.getMsg(room, (err, result7) => {console.log('>>> [svc] getMsg in');

	        	if(err) {
	        		console.log('>>> [svc] model.getMsg error!');
					return next(err);
	        	}
	        	console.log('>>> [router] mongo.getMsg success!');
				console.log('>>> [router] mongo.getMsg result : ', result7);
				socket.emit('pastMsg', result7);
	        });


	        // 개별 클라이언트에 환영 메세지 ( socket.emit : 해당 클라이언트에게 전송 )
	    	io.to(room).emit('toClient', {sender:'Admin', msg: user +' 님이 입장하였습니다. Room number : ' + room});
	    });
	     
	    socket.on('disconnect', function() {
	      	console.log('Disconnected');
	    });
	 });

}

