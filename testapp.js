var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var morgan = require('morgan');
var bodyParser = require("body-parser");
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var flash = require('connect-flash');



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(morgan('dev'));


app.use(require('./router/router'));
const chat = require('./service/chatService');



app.use(require('./goodsRouter'));
app.use(handleError);
app.set('view engine', 'ejs');




server.listen(3001, function(){
	console.log("Connected & Listen to port 3001");
});



function handleError(err, req, res, next){
 	
 	if(err.code)

 		res.status(err.code);
 	else
 		res.status(500);

	res.send({msg:err.message});
};

chat(server);
